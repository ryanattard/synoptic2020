﻿using Npgsql;
using ProgForCloudSynoptic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProgForCloudSynoptic.DataAccess
{
    public class ChatRepository: ConnectionClass
    {
        public ChatRepository(): base()
        {

        }

        public List<ChatMessage> GetMessages()
        {
            string sql = "Select id, sender, message, datesent from chats";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);


            MyConnection.Open();
            List<ChatMessage> results = new List<ChatMessage>();

            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    ChatMessage p = new ChatMessage();
                    p.SenderEmail = reader.GetString(1);
                    p.Message = reader.GetString(2);
                    p.DateSent = reader.GetDateTime(3);
                    results.Add(p);
                }
            }

            MyConnection.Close();

            return results.OrderBy(x=>x.DateSent).ToList();
        }
    }
}