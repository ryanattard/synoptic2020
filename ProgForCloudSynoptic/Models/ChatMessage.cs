﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProgForCloudSynoptic.Models
{
    public class ChatMessage
    {
        public string SenderEmail { get; set; }
        public DateTime DateSent { get; set; }
        public string Message { get; set; }
    }
}