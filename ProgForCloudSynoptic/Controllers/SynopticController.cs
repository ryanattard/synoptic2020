﻿using ProgForCloudSynoptic.DataAccess;
using ProgForCloudSynoptic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProgForCloudSynoptic.Controllers
{
    [Authorize]
    public class SynopticController : Controller
    {
        public ActionResult Index()
        {
            List<ChatMessage> messages = new List<ChatMessage>();
            //Task 1: You need to return all the existent Messages from the database and pass them to View to display them
            //...

            ChatRepository cr = new ChatRepository();
            messages = cr.GetMessages();

            return View(messages);

        }
        // GET: Synoptic
        [HttpPost]
        public ActionResult Post()
        {
            //Here You need to post to pub/sub repository
            return RedirectToAction("Index");
        }

        //Assume that this method is being called by a cron job
        public ActionResult Pull()
        {
            //Here You need to pull from Pub/sub repository and write into the database
            //You can create a table in your own database or one is created for you having these details of database:

            return RedirectToAction("Index");
        }
    }
}