﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProgForCloudSynoptic.Startup))]
namespace ProgForCloudSynoptic
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
